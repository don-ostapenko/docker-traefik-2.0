### Start

#### Commands for start work of your network
* Create a default network for your projects with the next command:
```
$ docker network create --driver=bridge --attachable --internal=false traefik-network
```

* Run for start containers.
```
$ docker-compose up -d
```

#### After run commands above, to you will be available
* "Traefik Dashboard": [traefik.localhost](https://traefik.localhost "Traefik's Dashboard")
* "Portainer Dashboard": [portainer.localhost](https://portainer.localhost "Portainer's Dashboard")

#### Docker skeleton for your new project
* Clone [this](https://bitbucket.org/don-ostapenko/docker-docker-skeleton/src/master/ "Docker skeleton") repository into root of your new project;
* Follow by steps specified into [README.md](https://bitbucket.org/don-ostapenko/docker-docker-skeleton/src/master/README.md "README.md")
